# Integration Testing 

## Description

A repository created for the **Organization** and **Management** of the **Integration Testing** between OBC and OPS to ensure **Compatibility** as far as the Environmental Campaigns are concerned. 

## Instructions 
We need to ensure that everything (every TM and TC) is tested and has the expected functionality. 

All **Services** and functionalities that need to be tested for compatibility from both sides are mentioned as **Issues** in this project. Each issue represents one Service or functionality. 

The two related subsystems (OBC and OPS) will update the issues regading the status of implementation of each element to be tested. Every time a Service/functionality is completely implemented from one side their **Completeness Label** will be added to the respective issue. The Labels that will be used in this project are mentioned in the [Labels section](#labels) below. 

When both sides have completely implemented a Service (so both labels will appear on the respective issue), the Service can be tested.

Once a sufficient number of Services is implemented from both sides a meeting should be arranged for **Testing** (@lab or @clean room) with at least one member from each Subsystem present.

After a Service is completely tested (following the [Testing Instructions](#testing-instructions) below) the `Tested` Label can be added to the respective issue.

> If either side changes something in a Service that has already been tested the change should be mentioned in the respective issue so that the other side gets informed, the `Tested` label should be removed from the issue and the Service should be tested again.

### Testing Instructions

#### Testing Elements
Elements to be tested:
1. The Services and specific subservices that are going to be used in each Campaign. 
2. General fields such as data types, sizes, enumeration values, header fields etc.

#### Cases

In order to ensure completeness of testing, some procedures-cases should be prepared (and mentioned as a comment in the respective issue) before the testing session. Possible cases:
1. What TM is expected after a specific TC.
2. All possible cases-forms of a TM or TC, with all possible argument and parameters. 

Only if the proper functionality of all cases is ensured, can a Service be considered completely tested and take the `Tested` Label. 

### Issues

Each [issue](https://gitlab.com/acubesat/ops/integration-testing/-/issues) of this project represents a Service or General Functionality that needs to be tested from both sides.  

In the description of each issue the respective subservices that are going to be used in the respective Campaign are mentioned. In order for a Service to be completely tested **ALL SUBSERVICES SHOULD BE TESTED** (all TMs and TCs) for all possible [Cases](#cases).

### Labels 

The tracking of completeness of a Service is handled by the following labels:
1.  ~"OPS::Complete" | `A Service is completely implemented in OPS side` (Label to be added by OPS members)
2.  ~"OBC::Complete" | `A Service is completely implemented in OBC side` (Label to be added by OBC members)
3.  ~"Tested" | `A Service has already been tested` (Label to be added by OPS/OBC members)
4. ~"Payload Qualification Campaign" | `A Service that will be used in the Payload Qual. Campaign` (Label to be added by OPS/OBC members)
5. ~"OBC - ADCS Env. Campaign" | `A Service that will be used in the OBC-ADCS Env. Campaign` (Label to be added by OPS/OBC members)

